# **Depracation Notice** #

This is the old repo, and is no longer maintained. For all purposes find the [new repo for the toolbox](https://links.auquan.com/auquantoolboxnew). 

All the issues and Pull requests should be created in the new repo.

Auquan provides an open source backtesting toolbox to develop your trading algorithms and backtest strategies.
